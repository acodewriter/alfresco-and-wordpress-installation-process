# Processus d'installation de Alfresco Community Edition et de Wordpress

Nous utiliserons docker-compose tout au long de ce tutorial.

## Docker installation

Je vous renvoie sur la [documentation de Docker](https://docs.docker.com/engine/install/) et suivant votre system d’exploitation vous y verez une méthode détaillée pour l’installer.

## Alfresco installation using Docker

Rendez-vous sur [ce site](https://github.com/git-guides/install-git) et choisissez votre système d’exploitation pour pouvoir installer git.

Positionnez vous sur votre repertoire de travail.
Pour notre cas c'est:
```bash
cd /home/opc/workspace
```
clonner le projet puis positionnez vous dans le repertoire docker compose de ce project:
```bash
git clone https://github.com/Alfresco/acs-deployment.git
cd acs-deployment/docker-compose
```
Lancez la commande ci dessous pour télécharger les images, récupérer toutes les dépendances, crééer chaque container puis démarrer le systeme.

```bash
docker-compose -f community-docker-compose.yml up

#or

sudo  docker-compose -f community-docker-compose.yml # si vous avez des problèmes de droits d'accés
```
Et voilà Alfresco qui est déployé sur votre serveur.

```bash
# Vous pouvez y acceder sur http://localhost:8080
#Identifiants:

#username: admin

#password: admin
```

## Wordpress installation

positionnez dans votre repertoire de travail.
Pour notre cas c'est:
```bash
cd /home/opc/workspace
mkdir wordpress
cd wordpress
touch docker-compose.yml
```
Copier et coller le contenu ci dessous dans le fichier docker-compose.yml

```yaml
version: '3.1'

services:

  wordpress:
    image: wordpress
    restart: always
    ports:
      - 8080:80
    environment:
      WORDPRESS_DB_HOST: db
      WORDPRESS_DB_USER: exampleuser
      WORDPRESS_DB_PASSWORD: examplepass
      WORDPRESS_DB_NAME: exampledb
    volumes:
      - wordpress:/var/www/html

  db:
    image: mysql:5.7
    restart: always
    environment:
      MYSQL_DATABASE: exampledb
      MYSQL_USER: exampleuser
      MYSQL_PASSWORD: examplepass
      MYSQL_RANDOM_ROOT_PASSWORD: '1'
    volumes:
      - db:/var/lib/mysql

volumes:
  wordpress:
  db:
```

Lancez la commande:
```bash
docker-compose up

#puis rendez vous dans votre navigateur sur l'url suivant: 
#http://localhost:8080
#or
#http://my_ip:8080
```

Vous verrer un écran comme ça

![choose wordpress language page](./assets/images/choose-wordpress-language.PNG)

Choisissez votre langue et cliquez sur continue.

![fill wordpress detail](./assets/images/wordpress-detail.PNG)

Remplissez ces champs et cliquez sur install wordpress, puis cliquez sur login

![wordpress login page](./assets/images/wordpress-login-page.PNG)   

Sur cette page renseigner votre nom d'utilisateur et votre mot de passe et connectez vous.

Voila wordpress installé sur votre machine.

## Étapes pour sauvegarder et restaurer les volumes de données WordPress

1. Sauvegardez vos volumes de données WordPress
    1. Identifiez votre volume nommé en listant tous les volumes nommés à l'aide de la commande ci-dessous.

        ```bash
        $ docker volume ls   
        output:
          
        DRIVER              VOLUME NAME
          
        local               wordpress_db_data
        ```

2. Exécutez la commande de sauvegarde comme ci-dessous.

```baash
$ docker run --rm -v wordpress_db_data:/var/lib/mysql -v /tmp:/backup alpine tar -cjf /backup/my_wordpress_database_backup.tar.bz2 -C /volume ./
```

3. Restaurer votre volume de données WordPress

```bash
$ docker run --rm -v wordpress_db_data:/var/lib/mysql -v /tmp:/backup alpine sh -c "rm -rf /volume/* /volume/..?* /volume/.[!.]* ; tar -C /volume/ -xjf /backup/my_wordpress_database_backup.tar.bz2"
```