# Mesure de l'audience

## Mesurer l'audience d'un site internet ?

Pour faire simple, c’est connaitre le nombre de visites (journalières, hebdomadaires, mensuelles, etc.) qu’il reçoit, le profil de ses visiteurs, la durée moyenne qu’ils passent sur les pages disponibles, celles qu’ils consultent le plus, les mots-clés qu’ils ont utilisés pour atterrir sur elles...

## Outil de mesure d'audience

Google, le moteur de recherche le plus utilisé au monde, met gratuitement à votre disposition plusieurs outils pour vous aider à gérer votre stratégie marketing sur le web (plus communément appelée « stratégie digitale »). Les plus connus : Google AdWords, une sorte de régie publicitaire permettant à un annonceur (vous par exemple) d’afficher un lien commercial sur les pages de recherche Google quand un utilisateur semble intéressé par son secteur d’activité (le vôtre donc), et Google Analytics.

Ce dernier, vous offre les outils gratuits dont vous avez besoin afin d'analyser des données pour votre entreprise de façon centralisée. Vous pouvez ainsi prendre des décisions plus avisées.

## Configuration de Google Analytics

En général, Google Analytics se compose de:

- **Un code JavaScript lorsqu’un utilisateur accède** à votre site Web, le code JavaScript est exécuté pour recueillir toutes les informations importantes sur le site lui-même, les informations sur le visiteur comme le navigateur utilisé pour trouver votre site Web, le système d’exploitation, la résolution d’écran et ainsi de suite.
- **Un service de collecte de données** Une fois toutes ces informations collectées par le code JavaScript, elles sont envoyées aux serveurs Google pour un traitement sous forme de petits paquets appelés hits. Ces hits sont envoyées chaque fois qu’un visiteur ouvre votre site Web.
- **Un traitement des données** Pendant le traitement des données, les serveurs Google Analytics transforment les données brutes en informations utiles. Par exemple, en classant vos visiteurs en fonction de leur emplacement, langue, résolution d’écran, type de périphérique, etc.

### **Etape 1 – Creation d’un compte Google Analytics**

La première chose que vous devez faire est de créer un compte Google Analytics et d’obtenir le code de suivi. Les étapes ci-dessous montrent comment vous inscrire à Google Analytics:

1. Commencez par naviguer vers la page d’accueil de [Google Analytics](https://www.google.com/analytics/) . Appuyez sur le bouton **Connexion**, situé en haut de la page, puis sélectionnez **Google Analytics**.
2. Vous serez redirigé vers la page de connexion. Entrez votre adresse e-mail Google et appuyez sur le bouton **Suivant**. Si vous n’avez pas encore de compte Google, créez-en un en appuyant sur le bouton **Créer un compte**.

   ![Creer compte google analytic](./../assets/images/creer-compte-google-analytic.png)

3. Sur la page suivante, entrez votre mot de passe et appuyez sur le bouton Connexion.

   ![Votre email](./../assets/images/votre-email-google-analytic.png)

4. Appuyez sur le bouton **Inscription** pour continuer.

   ![Inscription Google Analytics](./../assets/images/inscription-analytics.png)

5. Pour créer un nouveau compte, vous devez saisir les informations suivantes: 1. **Nom du compte** 2. **Nom du site Web** 3. **URL du site Web** 4. **Catégorie sectorielle** 5. **Fuseau horaire des rapports**

   ![Remplir champs inscriptions Google Analytic](./../assets/images/remplir-champs-inscription-google-analytic.png)

Remplissez toutes les informations requises et appuyez sur le bouton **Obtenir un ID de suive en bas de la page**. Sur la page suivante, vous serez invité à accepter les conditions d’utilisation, cliquez sur le bouton **J’accepte**.

C’est tout! Vous avez créé avec succès un compte Google Analytics. Sur la page suivante, vous verrez le code de suivi Google Analytics Universal:

```js
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
    ga('create', 'UA-00000000-1', 'auto');
    ga('send', 'pageview');
</script>
```

Ce code JavaScript est identique pour tous les sites Web. Seul l’ID de suivi ( **UA-00000000-1** ) est unique. Vous devriez voir le vôtre sur la même page. L’ID de suivi commence par **UA**, qui signifie Universal Analytics, le premier ensemble de chiffres représentant le numéro de compte ( **00000000** ) et le dernier numéro représentant l’ID de propriété Google Analytics ( **1** ).

### **Etape 2 – Ajout de Google Analytics à un blog WordPress**

Il existe plus d’une façon d’ajouter le code de suivi Google Analytics à WordPress: en utilisant un **Plugin**, en ajoutant un script de suivi **directement au fichier header.php** ou en créant une nouvelle fonction dans le fichier **functions.php**.

#### **Option 1 – Utilisation d’un plugin**

Les étapes ci-dessous montrent comment installer Google Analytics avec le plug-in et comment insérer l’ID de suivi :

1. Connectez-vous au Tableau de bord WordPress et installez le plugin **Google Analytics**.
2. Une fois l’installation terminée, accédez à la section **Paramètres** et appuyez sur **Google Analytics Code**
3. Collez votre code dans le champ et cliquez sur **Enregistrer**

![Collez votre code Google Analytic](./../assets/images/paste-access-code-google-analytic.png)

4. C’est tout, le code de suivi a été ajouté avec succès à votre blog WordPress.

Veuillez noter que Google Analytics peut prendre entre 12 et 24 heures pour être mis à jour et commencer à afficher les premiers résultats. Ensuite, des statistiques sur votre audience seront fournies dans l’onglet **Rapports** de Google Analytics

![Exemple Audience Google Analytic](./../assets/images/exemple-audience-google-analytic.png)

#### **Option 2 – Insertion du code dans header.php**

Il y a plusieurs façons d’intégrer le code Google Analytics à un blog WordPress. Par exemple, le code peut être inclus dans le fichier **header.php** de votre thème:

1. Accédez à la zone d’administration de WordPress et accédez à **Apparence** -> **Editeur**.
2. Ouvrez **header.php** pour l’édition.
3. Insérez le code de suivi Analytics que vous avez acquis à l’ **étape 1** avant la balise ```</head>``` et et appuyez sur le bouton **Mettre à jour le fichier**

![Add Google Analytic Code to header.ph file](./../assets/images/script-header-google-analytic.png)

**Remarque**: le code Analytics ne sera inséré que sur votre thème actif. Vous devrez l’ajouter à nouveau si vous changez le thème.

#### **Option 3 – Création d’une nouvelle fonction dans le fichier functions.php**

Cette méthode nécessite quelques connaissances fondamentales de codage et nous vous recommandons de la suivre seulement si vous êtes confiant par rapport à ce que vous faites. De plus, il est fortement recommandé de créer une **sauvegarde** du fichier **functions.php** ou même de toute l’installation de WordPress avant de continuer:

1. Dans le tableau de bord de WordPress, accédez à la section **Apparence** -> **Éditeur**.
2. **Sélectionnez functions.php** dans la liste des fichiers à droite.
3. L’extrait de code suivant crée une nouvelle fonction et insère le code de suivi Analytics juste avant la balise ```</head>``` de votre site WordPress. Collez ce code au bas du fichier **functions.php** , mais assurez-vous de changer votre ID de suivi.

```php
add_action('wp_head','my_analytics', 20);

function my_analytics() {
    ?>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-00000000-1', 'auto');
            ga('send', 'pageview');
        </script>
    <?php
}
```

4. Enregistrez les modifications que vous avez apportées en cliquant sur le bouton **Mettre à jour le fichier**.

![Add Google Analytic Code to functions.php file](./../assets/images/fonction-php-google-analytic.png)

**Remarque**: Le code sera inséré uniquement sur votre thème actif. Si vous modifiez votre thème, vous devrez ajouter à nouveau le code.

## references et webographie

- [Pourquoi et comment mesurer l'audience d'un site internet](https://pro.orange.fr/lemag/pourquoi-et-comment-mesurer-l-audience-d-un-site-internet-CNT000001zVzhb.html)
- [Top 10 des moteurs de recherche dans le monde](https://www.noiise.com/ressources/seo/top-10-des-moteurs-de-recherche-dans-le-monde/#:~:text=1.-,Google,81%25%20de%20part%20de%20march%C3%A9%20!)
- [Mesurer l'audience de son site web avec Google Analytics](https://www.abcportage.fr/portage-salarial/actualites/2017-02/comment-mesurer-laudience-de-son-site-web-avec-google-analytics/?TEST=B)
- [Comment installer Google Analytics sur WordPress facilement](https://www.hostinger.fr/tutoriels/google-analytics-wordpress)
