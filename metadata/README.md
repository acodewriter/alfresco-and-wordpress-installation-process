#  <u>Comparaison entre Google Workspace et Alfresco</u>

## <u>Metadata</u>
### Google Drive
Les ``Labels`` sont des métadonnées que vous définissez pour aider les utilisateurs à organiser, rechercher et appliquer des règles aux éléments dans Drive, Docs, Sheets et Slides.

## <u>Categorisation des fichiers</u>
### <u>Alfresco</u>
Alfresco est capable de gérer de multiples moyens de classification de documents tout en permettant une organisation adaptée à la nature des documents archivés (factures, contrats, rapports, courrier entrant et sortant, management de la qualité, fiches de procédures, documentation technique, etc.)
#### <u>Moyens de classements</u>
#####  <u>Arborescence</u>
Le classement des documents en arborescence se fait à travers le stockage du contenu sous des espaces ayant comme nom une catégorie ou un type bien défini de document.

Il s’agit en d’autres termes d’une organisation hiérarchique en espaces et sous-espaces semblable à l’organisation dans un système de fichiers classique.
![classification par arborescence](./../assets/images/arborescence-alfresco.png)
#####  <u>Tags</u>
Comme dans le cas des tags (mots-clés) d’un article de blog, un contenu peut être associé à plusieurs tags. Une fonctionnalité importante c’est d’exposer l’ensemble des tags en une vue dynamique tout en permettant de:

- Accéder à tous les document ayant un certain tag parmi leurs attributs
- Afficher le nombre total des documents marqués avec ce tag
![classification par tags](./../assets/images/tags-alfresco.png)
#####  <u>Catégories</u>
Alfresco permet aussi aux utilisateurs la possibilité de classer aisément les documents par catégories. Celles-ci sont organisées d’une manière hiérarchique tout en fournissant ainsi une recherche de contenu plus performante.
![classification par categories](./../assets/images/categories-alfresco.png)
### Recapitulatif
<pre>
    <table>
        <tr>
            <th></th>
            <th>Arborescence</th>
            <th>Tags</th>
            <th>Catégories</th>
        </tr>
        <tr>
            <th>Controle d'acces</th>
            <td style="text-align:center;">X</td>
            <td style="text-align:center;">_</td>
            <td style="text-align:center;">_</td>
        </tr>
        <tr>
            <th>Console d'administration</th>
            <td style="text-align:center;">_</td>
            <td style="text-align:center;">X</td>
            <td style="text-align:center;">X</td>
        </tr>
        <tr>
            <th>Hierarchisable</th>
            <td style="text-align:center;">X</td>
            <td style="text-align:center;">_</td>
            <td style="text-align:center;">X</td>
        </tr>
        <tr>
            <th>Multi-critere</th>
            <td style="text-align:center;">_</td>
            <td style="text-align:center;">X</td>
            <td style="text-align:center;">X</td>
        </tr>
        <tr>
            <th>Acces rapide</th>
            <td style="text-align:center;">_</td>
            <td style="text-align:center;">X</td>
            <td style="text-align:center;">_</td>
        </tr>
    </table>
</pre>
### <u>Google Drive</u>
Les ``labels`` sont des métadonnées que vous définissez pour aider les utilisateurs à organiser, rechercher et appliquer des règles aux éléments dans Drive, Docs, Sheets et Slides. Les labels de Drive sont utiles pour de nombreux scénarios courants sur le lieu de travail pour l'organisation des fichiers, notamment : la gestion des enregistrements, la classification, la recherche structurée, le flux de travail, la création de rapports, l'audit, etc.

#### Pourquoi utiliser les labels

* Classifier le contenu pour suivre une stratégie de gouvernance des informations
Vous pouvez créer une étiquette qui correspond à un schéma de classification qui peut être appliqué de manière cohérente dans toute votre organisation pour identifier le contenu sensible ou le contenu nécessitant un traitement spécial.
* Appliquer la politique aux éléments
Les labels de Drive peuvent être utilisées comme condition ou action dans [les règles de prévention contre la perte de données](https://support.google.com/a/answer/9843931) (bêta) ou [les règles de conservation Vault](https://support.google.com/vault/answer/2990828) (à venir) pour répondre aux exigences de conformité. 
Les règles vous permettent d'utiliser des labels pour vous assurer que le contenu Drive est géré tout au long de son cycle de vie et respecte les pratiques d'archivage de votre organisation.
* Organisez et trouvez des fichiers plus rapidement
Vous pouvez créer des labels qui augmentent la capacité de recherche des connaissances de votre entreprise. Les utilisateurs travaillant avec des fichiers dans un environnement collaboratif peuvent utiliser des labels qu'ils appliquent comme attributs supplémentaires pour améliorer la recherche et l'organisation du contenu.
Les membres de votre organisation peuvent alors rechercher du contenu en fonction des labels et des champs.
## <u>Performence de recherche de documents</u>

### <u>Alfresco</u>

Trouvez rapidement le document recherché dans votre système de gestion parmi des milliers, voire des centaines de millions de fichiers.

* Alfresco Query Accelerator utilise des ensembles de requêtes fonctionnels pour améliorer les performances de recherche en interrogeant les propriétés et aspects des métadonnées sur de gros volumes d'informations.

* Des fonctionnalités de recherche puissantes, intégrant notamment des suggestions de recherche et des filtres, permettent de retrouver rapidement le contenu pertinent.

* [Alfresco Federation Services](https://www.alfresco.com/fr/afs) offre un moyen simple de rechercher et gérer du contenu dans plus de 60 applications métiers et systèmes de gestion de contenu d'entreprise parmi les plus courants (par exemple, Documentum, OpenText et IBM FileNet)

### <u>Google Drive</u>
Pour trouver des fichiers dans Google Drive, Docs, Sheets et Slides, effectuez une recherche par:
* Titre du fichier
* Contenu du fichier
* Type de fichier
* Autres métadonnées, y compris :
    * Champ "Description"
    * Libellés partagés
    * Emplacement du fichier
    * Propriétaire
    * Créateur
    * Date de dernière modification
* Éléments ou mots figurant dans des images, des fichiers PDF ou d'autres fichiers stockés sur votre Drive.

Vous pouvez également trier et filtrer les résultats de recherche.

## <u>Integration sur Wordpress</u>

### <u>Google Drive</u>
* Integrate Google Drive: est le meilleur plugin de solution cloud Google Drive pour WordPress, facile à utiliser, pour intégrer vos documents et médias Google Drive directement dans votre site Web WordPress.
Partagez vos fichiers cloud Google Drive sur votre site très rapidement et facilement.
Vous pouvez parcourir, gérer, intégrer, afficher, télécharger, télécharger, rechercher, lire, partager vos fichiers Google Drive directement sur votre site Web sans aucun tracas ni codage.

### <u>Alfresco</u>
* AlfrescoDoc: est un plugin Wordpress pour afficher des documents depuis un serveur Alfresco. Il est :
    * Facile à afficher les métadonnées des documents et des dossiers à partir du serveur Alfresco.
    * Utilisation de l'API Alfresco CMIS
    * Il est possible de contrôler les métadonnées à afficher.
### <u>Références, Webographies</u>
- [Manage Drive Labels](https://support.google.com/a/answer/9292382?hl=en&ref_topic=2490099)
- [Comment classer vos documents dans Alfresco](http://archiveyourdocs.com/comment-classer-les-documents-dans-alfresco)
- [Recheche à grande echelle Alfresco](https://www.alfresco.com/fr/ecm-software)
- [Recherche sur Google Drive](https://support.google.com/drive/answer/2375114)
- [Avantages de Google Workspace](https://hellocloud.ma/blogs/les-avantages-de-g-suite-pour-votre-entreprise/)
- [Gestion Tags dans Alfresco Share](http://archiveyourdocs.com/gestion-des-tags-dans-alfresco-share-2)

- [Integrer Drive sur Wordpress](https://wordpress.org/plugins/integrate-google-drive/)
- [Integrer Alfresco sur Wordpress](https://github.com/MajesticComputerTechnology/alfrescodoc-wordpress)