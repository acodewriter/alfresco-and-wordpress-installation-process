# Installation des extensions sur Wordpress

## Exemple de l'installation de l'extension Qgis-map

Connectez-vous à votre espace admininistration par  example : 

```bash
http://localhost/wordpress/wp-admin
```

Maintenant que vous etes connecté à votre dashboard, cliquez sur **plugins>add New**

Sur la bar de recherche tapez **Qgis maps** et choisissez : 

![Qgis maps](./../assets/images/qgis-maps.PNG)

Appuyez sur le boutton **Install Now**. Et voilà l'installation de qgis mapas faite.
Ensuite vous pouvez appuyer sur **activate** pour activer l'extension installé.

Ainsi ce procédé reste valable pour les autres extensions comme : 

- Fantastic ElasticSearch
- Jetpack
- LayerSlider WP
- MonsterInsights - Google Analytics pour WordPress
- Smush
- Yoast SEO
- Wordfence Security
- Contact Form 7
- Akismet Anti-Spam